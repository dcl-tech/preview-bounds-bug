Demo Boundary Bug

Carl Fravel

This script shows a scene-boundary-exceeded false positive

It only occurs with dcl start preview, or subsequent Ctrl+F5 reloads of the preview scene.

The false positive disappears if you touch the scene and see an auto reload, or if you deploy it.