/////////////////////////////////////////////////////
// Demo Boundary Bug
// Carl Fravel
// This script shows a scene-boundary-exceeded false positive
// It only occurs with dcl start preview, or subsequent Ctrl+F5 reloads of the preview scene.
// The false positive disappears if you touch the scene and see an auto reload, or if you deploy it.
/////////////////////////////////////////////////////

class DemoPanel extends Entity {
    constructor(x:number,y:number,z:number,  xRot:number, yRot:number, zRot:number,  xScale:number, yScale: number, zScale:number){
        super()
        this.addComponent(new Transform({position:new Vector3(x,y,z), rotation: Quaternion.Euler(xRot,yRot,zRot), scale:new Vector3(xScale, yScale,zScale)}))
        this.addComponent(new BoxShape())
        engine.addEntity(this)
    }
}

let demoParentEntity = new Entity()
demoParentEntity.addComponent(new Transform({position:new Vector3(4,0,4), rotation: Quaternion.Euler(0,0,0), scale:new Vector3(1,1,1)}))
engine.addEntity(demoParentEntity)
    
let demoPanel = new DemoPanel(0,1,0, 0,90,-45, 1,0.03,1.7)
demoPanel.setParent(demoParentEntity)
